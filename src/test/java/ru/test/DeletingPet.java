package ru.test;

import io.restassured.http.ContentType;
import model.EndPoints;
import model.JsonData;
import model.JsonMessageData;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class DeletingPet {

    private int existingPetId = 1234;
    private String notValidPetId = "test";

    @Test
    public void test0DeleteExistingPetOK() {
        AddingPets pets = new AddingPets();
        pets.test1AddNewPetOK();
        given()
                .baseUri(EndPoints.mainUrl)
                .when()
                .delete(EndPoints.pets + existingPetId)
                .then()
                .statusCode(200)
                .assertThat().body(equalTo(JsonMessageData.deletedPet));
    }

    @Test (dependsOnMethods = { "test0DeleteExistingPetOK" })
    public void test1DeleteExistingPetFail() {
        given()
                .baseUri(EndPoints.mainUrl)
                .when()
                .delete(EndPoints.pets + existingPetId)
                .then()
                .statusCode(404);
    }

    @Test
    public void test2DeleteExistingPetFail() {
        given().log().all()
                .baseUri(EndPoints.mainUrl)
                .when()
                .delete(EndPoints.pets + notValidPetId)
                .then()
                .statusCode(404)
                .assertThat().body(equalTo(JsonMessageData.errorIdPet));
    }

    private void addPet() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(JsonData.jsonNewPet)
                .when()
                .post(EndPoints.pets)
                .then()
                .statusCode(200)
                .assertThat().body(equalTo(JsonData.jsonNewPet));
    }
}
