package ru.test;

import io.restassured.http.ContentType;
import model.EndPoints;
import model.JsonData;
import model.JsonMessageData;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class AddingPets {

    @Test
    public void test1AddNewPetOK() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(JsonData.jsonNewPet)
                .when()
                .post(EndPoints.pets)
                .then()
                .statusCode(200)
                .assertThat().body(equalTo(JsonData.jsonNewPet));
    }

    @Test
    public void test2AddNewPetWithoutDataFailed() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .when()
                .post(EndPoints.pets)
                .then()
                .statusCode(405)
                .assertThat().body(equalTo(JsonMessageData.errorNoBodyData));
    }
}
