package ru.test;

import io.restassured.http.ContentType;
import model.EndPoints;
import model.JsonData;
import model.JsonMessageData;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class UpdatingPet {
    @Test
    public void test0UpdatePetOK() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .body(JsonData.jsonUpdatePet)
                .when()
                .put(EndPoints.pets)
                .then()
                .statusCode(200)
                .assertThat().body(equalTo(JsonData.jsonUpdatePet));
    }

    @Test
    public void test1UpdateNewPetFailed() {
        given()
                .baseUri(EndPoints.mainUrl)
                .contentType(ContentType.JSON)
                .when()
                .put(EndPoints.pets)
                .then()
                .statusCode(405)
                .assertThat().body(equalTo(JsonMessageData.errorNoBodyData));
    }
}
