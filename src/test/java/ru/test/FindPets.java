package ru.test;

import model.EndPoints;
import model.JsonData;
import model.JsonMessageData;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class FindPets {

    private int existingPetId = 1234;
    private int notExistingPetId = 8676827;
    private String notValidPetId = "test";

    private AddingPets pets = new AddingPets();

    @Test
    public void test1FindExistingPetByCorrectId() {

        pets.test1AddNewPetOK();
        given()
                .baseUri(EndPoints.mainUrl)
                .when()
                .get(EndPoints.pets + existingPetId)
                .then()
                .statusCode(200)
                .assertThat().body(equalTo(JsonData.jsonNewPet));
    }

    @Test
    public void test2FindPetByNonExistId() {
        given()
                .baseUri(EndPoints.mainUrl)
                .when()
                .get(EndPoints.pets + notExistingPetId)
                .then()
                .statusCode(404)
                .assertThat().body(equalTo(JsonMessageData.notFoundPet));
    }

    @Test
    public void test3FindPetByIncorrectId() {
        given().log().all()
                .baseUri(EndPoints.mainUrl)
                .when()
                .get(EndPoints.pets + notValidPetId)
                .then()
                .statusCode(404)
                .assertThat().body(equalTo(JsonMessageData.errorIdPet));
    }
}
