package model;

public final class EndPoints {
    public static final String mainUrl = "https://petstore.swagger.io/v2/";
    public static final String pets = "pet/";
}
