package model;

public class JsonMessageData {
    public static String notFoundPet =  "{\"code\":1,\"type\":\"error\",\"message\":\"Pet not found\"}";
    public static String deletedPet =  "{\"code\":200,\"type\":\"unknown\",\"message\":\"1234\"}";
    public static String errorNoBodyData = "{\"code\":405,\"type\":\"unknown\",\"message\":\"no data\"}";
    public static String errorIdPet = "{\"code\":404,\"type\":\"unknown\",\"message\":\"java.lang.NumberFormatException: For input string: \\\"test\\\"\"}";
}
