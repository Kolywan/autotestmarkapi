package model;

public class JsonData {

    public static String jsonNewPet = "{\"id\":1234,\"category\":{\"id\":0,\"name\":\"doggie\"},\"name\":\"doggie\",\"photoUrls\":[\"string\"],\"tags\":[{\"id\":0,\"name\":\"string\"}],\"status\":\"available\"}";
    public static String jsonUpdatePet = "{\"id\":1234,\"category\":{\"id\":0,\"name\":\"dog\"},\"name\":\"dog\",\"photoUrls\":[\"some URL\"],\"tags\":[{\"id\":0,\"name\":\"tags\"}],\"status\":\"pending\"}";

}
